FROM php:5.6-apache

RUN a2enmod rewrite expires

# install the PHP extensions we need
RUN apt-get update && apt-get install -y libpng-dev libjpeg-dev libkrb5-dev \
    && docker-php-ext-configure gd --with-png-dir=/usr --with-jpeg-dir=/usr \
    && docker-php-ext-install gd mysqli \
    && apt-get -y install libssl-dev libc-client2007e-dev libkrb5-dev \
    && docker-php-ext-configure imap --with-imap-ssl --with-kerberos \
    && docker-php-ext-install imap opcache \
    && rm -rf /var/lib/apt/lists/*

# setting the recommended for vtiger
RUN { \
        echo 'display_errors=On'; \
        echo 'max_execution_time=0'; \
        echo 'error_reporting=E_WARNING & ~E_NOTICE & ~E_DEPRECATED'; \
        echo 'log_errors=Off'; \
        echo 'short_open_tag=On'; \
    } > /usr/local/etc/php/conf.d/vtiger-recommended.ini

# setting the reccomended for opcache
# https://secure.php.net/manual/en/opcache.installation.php
RUN { \
        echo 'opcache.memory_consumption=128'; \
        echo 'opcache.interned_strings_buffer=8'; \
        echo 'opcache.max_accelerated_files=4000'; \
        echo 'opcache.revalidate_freq=60'; \
        echo 'opcache.fast_shutdown=1'; \
        echo 'opcache.enable_cli=1'; \
    } > /usr/local/etc/php/conf.d/opcache-recommended.ini

#install xdebug 2.5.5 per Php5.6
RUN pecl install xdebug-2.5.5

RUN apt-get update -y
RUN apt-get install -y nano

# quello che viene fatto con echo è la scrittura della configurazione nel file /usr/local/etc/php/php.ini
#setting php.ini per xdebug
#  RUN yes | pecl install xdebug-2.5.5 \
RUN echo ";Added for xdebug" >> /usr/local/etc/php/php.ini \
      && echo "zend_extension=/usr/local/lib/php/extensions/no-debug-non-zts-20131226/xdebug.so" >> /usr/local/etc/php/php.ini \
      && echo "xdebug.default_enable=1" >> /usr/local/etc/php/php.ini \
      && echo "xdebug.remote_autostart=1" >> /usr/local/etc/php/php.ini \
      && echo "xdebug.remote_connect_back=1" >> /usr/local/etc/php/php.ini \
      && echo "xdebug.remote_enable=1" >> /usr/local/etc/php/php.ini \
      && echo "xdebug.remote_port=9000" >> /usr/local/etc/php/php.ini \
      && echo "xdebug.idekey=netbeans-xdebug" >> /usr/local/etc/php/php.ini \
      && echo "xdebug.remote_handler=dbgp" >> /usr/local/etc/php/php.ini

#fine installazione xdebug	

WORKDIR /var/www/html

CMD ["apache2-foreground"]
